# The Stock Trader

### A Vue.js project using:

- vuex
- vue-router
- vue-resourse
- firebase
- Bootstrap 3

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
